ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG CONFLUENT_KAFKA_VERSION
ARG CONFLUENT_COMMON_VERSION
ARG CONFLUENT_REST_UTILS_VERSION
ARG CONFLUENT_SCHEMA_REGISTY_VERSION
ARG CONFLUENT_CONNECT_STORAGE_COMMON_VERSION

ENV MAVEN_OPTS="-Xms512m -Xmx7000m -XX:ReservedCodeCacheSize=1g"

RUN \
    set eux && \
    apt update && apt install -y grep git bash curl python3 wget libc6 && \
    pwd

RUN \
    set eux && \
    rm -rf ./kafka && \
    git clone https://github.com/confluentinc/kafka/ && \
    cd kafka && \
    git fetch origin refs/tags/v6.0.3:refs/tags/v6.0.3 && \
    git checkout tags/v6.0.3 && \
    export GRADLE_OPTS="-Xmx7g -XX:MaxMetaspaceSize=768m -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8" && \
    chmod +x ./gradlewAll && \
    chmod +x ./gradlew && \
    ./gradlew clean && \
    ./gradlewAll -PscalaVersion=2.12 install

RUN \
    set eux && \
    rm -rf ./kafka && \
    git clone https://github.com/confluentinc/kafka/ && \
    cd kafka && \
    git fetch origin refs/tags/v$CONFLUENT_KAFKA_VERSION:refs/tags/v$CONFLUENT_KAFKA_VERSION && \
    git checkout tags/v$CONFLUENT_KAFKA_VERSION && \
    export GRADLE_OPTS="-Xmx7g -XX:MaxMetaspaceSize=768m -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8" && \
    chmod +x ./gradlewAll && \
    chmod +x ./gradlew && \
    ./gradlew clean && \
    ./gradlewAll -PscalaVersion=2.12 install

RUN \
    set eux && \
    rm -rf ./common && \
    git clone https://github.com/confluentinc/common/ && \
    cd common && \   
    git fetch origin refs/tags/v6.0.3:refs/tags/v6.0.3 && \
    git checkout tags/v6.0.3 && \
    mvn clean install -DskipTests

RUN \
    set eux && \
    rm -rf ./common && \
    git clone https://github.com/confluentinc/common/ && \
    cd common && \   
    git fetch origin refs/tags/v$CONFLUENT_COMMON_VERSION:refs/tags/v$CONFLUENT_COMMON_VERSION && \
    git checkout tags/v$CONFLUENT_COMMON_VERSION && \
    mvn clean install -DskipTests
        
RUN \
    set eux && \
    rm -rf ./rest-utils && \
    git clone https://github.com/confluentinc/rest-utils/ && \
    cd rest-utils && \   
    git fetch origin refs/tags/v$CONFLUENT_REST_UTILS_VERSION:refs/tags/v$CONFLUENT_REST_UTILS_VERSION && \
    git checkout tags/v$CONFLUENT_REST_UTILS_VERSION && \
    mvn clean install -DskipTests

RUN \
    set eux && \
    git clone https://github.com/confluentinc/schema-registry/ && \
    cd schema-registry && \   
    git fetch origin refs/tags/v6.0.3:refs/tags/v$6.0.3 && \
    git checkout tags/v$6.0.3 && \
    mvn clean install -DskipTests

RUN \
    set eux && \
    rm -rf schema-registry && \
    git clone https://github.com/confluentinc/schema-registry/ && \
    cd schema-registry && \   
    git fetch origin refs/tags/v$CONFLUENT_SCHEMA_REGISTY_VERSION:refs/tags/v$CONFLUENT_SCHEMA_REGISTY_VERSION && \
    git checkout tags/v$CONFLUENT_SCHEMA_REGISTY_VERSION && \
    mvn clean install -DskipTests

RUN \
    set eux && \
    git clone https://github.com/confluentinc/kafka-connect-storage-common/ && \
    cd kafka-connect-storage-common && \
    git fetch origin refs/tags/v$CONFLUENT_CONNECT_STORAGE_COMMON_VERSION:refs/tags/v$CONFLUENT_CONNECT_STORAGE_COMMON_VERSION && git checkout tags/v$CONFLUENT_CONNECT_STORAGE_COMMON_VERSION && \
    wget https://public.nexus.pentaho.org/content/groups/omni/org/pentaho/pentaho-aggdesigner-algorithm/5.1.5-jhyde/pentaho-aggdesigner-algorithm-5.1.5-jhyde.jar && \
    wget https://public.nexus.pentaho.org/content/groups/omni/org/pentaho/pentaho-aggdesigner-algorithm/5.1.5-jhyde/pentaho-aggdesigner-algorithm-5.1.5-jhyde.pom && \
    mvn install:install-file -Dfile=./pentaho-aggdesigner-algorithm-5.1.5-jhyde.pom -DpomFile=pentaho-aggdesigner-algorithm-5.1.5-jhyde.pom && \
    mvn install:install-file -Dfile=./pentaho-aggdesigner-algorithm-5.1.5-jhyde.jar -DgroupId=org.pentaho -DartifactId=pentaho-aggdesigner-algorithm -Dversion=5.1.5-jhyde -Dpackaging=jar && \
    mvn clean install -DskipTests

